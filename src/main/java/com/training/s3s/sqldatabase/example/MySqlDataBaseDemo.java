package com.training.s3s.sqldatabase.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MySqlDataBaseDemo {

	public static void main(String[] args) {
		SpringApplication.run(MySqlDataBaseDemo.class, args);
	}

}
