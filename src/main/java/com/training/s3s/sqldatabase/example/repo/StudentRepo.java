package com.training.s3s.sqldatabase.example.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.training.s3s.sqldatabase.example.model.Student;



@Repository
public interface StudentRepo extends JpaRepository<Student, Integer> {
	
	public Student findByIdAndName(int id,String name);
	
	
	
}
